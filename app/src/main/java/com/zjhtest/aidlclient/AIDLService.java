package com.zjhtest.aidlclient;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class AIDLService extends Service {
    private static final String TAG = "AIDLService";
    private List<Book> mBooks = new ArrayList<>();

    private final BookManager.Stub mBookManager = new BookManager.Stub() {
        @Override
        public List<Book> getBooks() throws RemoteException {
            synchronized (this) {
                Log.d(TAG, "invoking server getBook method, now the list is " + mBooks);
                if (mBooks != null) {
                    return mBooks;
                }
                return new ArrayList<>();
            }
        }

        @Override
        public void addBook(Book book) throws RemoteException {
            synchronized (this) {
                if (book == null) {
                    Log.d(TAG, "Book is null from client (In)");
                    book = new Book("defaultBook", 88);
                }

                if (!mBooks.contains(book)) {
                    mBooks.add(book);
                }

                Log.d(TAG, "invoking server addBook method, now the list is " + mBooks);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Book initBook = new Book("initBook", 22);
        mBooks.add(initBook);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, String.format("onBind execute, intent is %s", intent.toString()));
        return mBookManager;
    }
}
